#!/bin/bash

#Global variables
title="Choose an option"
backtitle="CNA432: Kevin Clark -- Linux assignment 1"
menu="25 78 5"
back="Exit quiz, go to menu"

quizScore()
{
	whiptail --fb --backtitle "$backtitle" \
	--nocancel --msgbox "You scored $1 point(s) out of 7" \
	25 78 16
}

quiz()
{
	score=0
	incorrect="Sorry, your answer \""
	incorrect2="\" is incorrect.\n\n"
	correct="Your answer \""
	correct2="\" is correct.\n\n"
	
	for i in $(seq 1 7); do
		case $i in
			1) ##############################################################
				questionText="What is the command to make sure 'scriptfile' is world executable and at the same time you and the group have full control?"
				ans=$(
					whiptail --fb --cancel-button Exit --title "Question $i" \
					--backtitle "$backtitle" --menu "$questionText" $menu \
					"A." "chmod ug+rwx,o+x scriptfile" \
					"B." "chmod 777 scriptfile" \
					"C." "chmod scriptfile 777" \
					"D." "chmod scriptfile ug+rwx,o+x" \
					"<--" "$back" \
					3>&1 1>&2 2>&3
				)
				msgtext="The correct command is 'chmod ug+rwx,o+x scriptfile'. The 'chmod' command uses syntax 'chmod permission-set file', not the other way around. In addition, 777 is full permissions, and giving a file 777 permissions is bad security."
				case $ans in
					"")
						exit
						;;
					"<--")
						break
						;;
					"A.")
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${correct}${ans}${correct2}${msgtext}" \
						25 78 16
						score=$(expr $score + 1)
						;;
					*)
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${incorrect}${ans}${incorrect2}${msgtext}" \
						25 78 16
						;;
				esac
				;;
			2) ##############################################################
				questionText="If you see the permissions for a file are 'rwxr-xr-x', who can write to it?"
				ans=$(
					whiptail --fb --cancel-button Exit --title "Question $i" \
					--backtitle "$backtitle" --menu "$questionText" $menu \
					"A." "User, group, and other" \
					"B." "User and group" \
					"C." "User only" \
					"D." "Nobody" \
					"<--" "$back" \
					3>&1 1>&2 2>&3
				)
				msgtext="The letter 'w' means write in a permission set. As a reminder, permissions are laid out where the user has the first 3 permission bits, the group gets the middle 3, and other has the right 3 bits. Since the w is on the leftmost part of the permission set, only the user has write access."
				case $ans in
					"")
						exit
						;;
					"<--")
						break
						;;
					"C.")
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${correct}${ans}${correct2}${msgtext}" \
						25 78 16
						score=$(expr $score + 1)
						;;
					*)
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${incorrect}${ans}${incorrect2}${msgtext}" \
						25 78 16
						;;
				esac
				;;
			3) ##############################################################
				questionText="If you see the permissions for a file are -rwxrwxrwx, who can write to it?"
				ans=$(
					whiptail --fb --cancel-button Exit --title "Question $i" \
					--backtitle "$backtitle" --menu "$questionText" $menu \
					"A." "Other" \
					"B." "Group, other" \
					"C." "User, group" \
					"D." "User, group, other" \
					"<--" "$back" \
					3>&1 1>&2 2>&3
				)
				msgtext="Each of user, group, and other have the 'w' for write in their sections of the permission set. It's all 3."
				case $ans in
					"")
						exit
						;;
					"<--")
						break
						;;
					"D.")
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${correct}${ans}${correct2}${msgtext}" \
						25 78 16
						score=$(expr $score + 1)
						;;
					*)
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${incorrect}${ans}${incorrect2}${msgtext}" \
						25 78 16
						;;
				esac
				;;
			4) ##############################################################
				questionText="You see that the permissions on the file 'textfile' are '-rw-------'. What command would you issue to make this file have the same permissions as the file in question 3: '-rwxrwxrwx'?"
				ans=$(
					whiptail --fb --cancel-button Exit --title "Question $i" \
					--backtitle "$backtitle" --menu "$questionText" $menu \
					"A." "chmod go+rwx textfile" \
					"B." "chmod 000 textfile" \
					"C." "chmod a+rwx testfile" \
					"D." "chmod 7777 testfile" \
					"<--" "$back" \
					3>&1 1>&2 2>&3
				)
				msgtext="'chmod a+rwx testfile' is the correct answer. This is another way to give all (standard) permissions to a file. Option D is not correct because it uses 7777 instead of 777."
				case $ans in
					"")
						exit
						;;
					"<--")
						break
						;;
					"C.")
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${correct}${ans}${correct2}${msgtext}" \
						25 78 16
						score=$(expr $score + 1)
						;;
					*)
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${incorrect}${ans}${incorrect2}${msgtext}" \
						25 78 16
						;;
				esac
				;;
			5) ##############################################################
				questionText="What's the command to issue to make sure everybody in your group and yourself have read and write access to the file 'textfile' but the world can only read it?"
				ans=$(
					whiptail --fb --cancel-button Exit --title "Question $i" \
					--backtitle "$backtitle" --menu "$questionText" $menu \
					"A." "chmod 644 textfile" \
					"B." "chmod 664 textfile" \
					"C." "chmod 666 textfile" \
					"D." "chmod ug+rw textfile" \
					"<--" "$back" \
					3>&1 1>&2 2>&3
				)
				msgtext="The answer is B. Allowing 6 (rw) for both user and group, but only 4 (r) for the world."
				case $ans in
					"")
						exit
						;;
					"<--")
						break
						;;
					"B.")
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${correct}${ans}${correct2}${msgtext}" \
						25 78 16
						score=$(expr $score + 1)
						;;
					*)
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${incorrect}${ans}${incorrect2}${msgtext}" \
						25 78 16
						;;
				esac
				;;
			6) ##############################################################
				questionText="You want everybody in the world to be able to execute your file 'progfile'. What is the command to accomplish this?"
				ans=$(
					whiptail --fb --cancel-button Exit --title "Question $i" \
					--backtitle "$backtitle" --menu "$questionText" $menu \
					"A." "chmod 111 progfile" \
					"B." "chmod 888 progfile" \
					"C." "chmod 666 progfile" \
					"D." "chmod 555 progfile" \
					"<--" "$back" \
					3>&1 1>&2 2>&3
				)
				msgtext="'chmod 555 progfile' is the correct answer. 555 is the only octal in the list of options that contains both write and execute. Write permission is needed to execute files because a file must be read before being executed."
				case $ans in
					"")
						exit
						;;
					"<--")
						break
						;;
					"D.")
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${correct}${ans}${correct2}${msgtext}" \
						25 78 16
						score=$(expr $score + 1)
						;;
					*)
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${incorrect}${ans}${incorrect2}${msgtext}" \
						25 78 16
						;;
				esac
				;;
			7) ##############################################################
				questionText="For webmasters: Why do you suppose cgi scripts are usually chmod 755?"
				ans=$(
					whiptail --fb --cancel-button Exit --title "Question $i" \
					--backtitle "$backtitle" --menu "$questionText" $menu \
					"A." "CGI scripts need are meant to be used by everyone" \
					"B." "CGI scripts need are meant to be used by admins only" \
					"C." "CGI scripts won't load unless permissions are 755" \
					"D." "RFC8446 states CGI scripts must have 755 permissions" \
					"<--" "$back" \
					3>&1 1>&2 2>&3
				)
				msgtext="CGI scripts are scripts used by any web user. To be executed by anyone, everyone must have read and execute permission. Only the admin should be able to edit (write) the file."
				case $ans in
					"")
						exit
						;;
					"<--")
						break
						;;
					"A.")
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${correct}${ans}${correct2}${msgtext}" \
						25 78 16
						score=$(expr $score + 1)
						;;
					*)
						whiptail --fb --backtitle "$backtitle" \
						--nocancel --msgbox "${incorrect}${ans}${incorrect2}${msgtext}" \
						25 78 16
						;;
				esac
				;;
		esac
	done
	quizScore $score
}

info()
{
	case $1 in
		"1.")
			whiptail --fb --backtitle "$backtitle" \
			--nocancel --msgbox "Standard UNIX/Linux permissions are read, write, and execute. For each set of 3 permissions, there are 3 groups to assign permissions: user, the owner of the file, group, the group that owns the file, and other, everyone else. This comes out to 9 total permission bits. If the user has read, write, and execute (rwx), the group only has read and execute (r-x), and everyone else has no permissions(---), then the permissions set looks like this: rwxr-x---" 25 78 16
			;;
		"2.")
			whiptail --fb --backtitle "$backtitle" \
			--nocancel --msgbox "Permissions of a file can be viewed with the 'stat file' command or 'ls -l file'. In the output of these commands, notice the symbolic notation that was explained in the 'Permissions' page. The 'stat' command also shows the octal representation of permissions denoted by the group of 4 numbers." 25 78 16
			;;
		"3.")
			whiptail --fb --backtitle "$backtitle" \
			--nocancel --msgbox "Octal notation is another way of representing permissions. In octal notation, the read permission (r) is worth 4, write (w) is worth 2, and execute (x) is worth 1. Add up the values of 'on' permissions from each section (user, group, other). The symbolic notation rwxr-x--- converts to 750 in octal notation." 25 78 16
			;;
		"4.")
			whiptail --fb --backtitle "$backtitle" \
			--nocancel --msgbox "Use the 'chmod' command to change the permissions of files and directories. The user can specify octal notation to change the file permissions: 'chmod 750 example-file'.  They can also use symbolic notation for permission changes: 'chmod u+x example-file'. For symbolic, use a combination of u, g, or o for target of permission change, '+' or '-' to add or remove the permission, and r, w, or x for the permission to change." 25 78 16
			;;
		"5.")
			quiz
			;;
		esac
}

main()
{
	while [ true ]; do
		ans=$(
			whiptail --fb --cancel-button Exit \
			--title "$title" \
			--backtitle "$backtitle" \
			--menu " " $menu \
			"1." "Permissions 101" \
			"2." "Viewing permissions" \
			"3." "Octal notation" \
			"4." "'chmod' command" \
			"5." "Take the quiz" \
			3>&1 1>&2 2>&3
		)
		if [ -z "$ans" ]; then
			exit
		else
			info $ans
		fi
	done
}

main